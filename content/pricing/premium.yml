---
  title: Why GitLab Premium?
  description: Enhance team productivity and coordination with GitLab Premium.
  side_menu:
    anchors:
      text: On this page
      data:
      - text: Overview
        href: "#overview"
        data_ga_name: overview
        data_ga_location: side-navigation
        nodes:
        - text: Summary
          href: "#wp-summary"
          data_ga_name: summary
          data_ga_location: side-navigation
        - text: Key solutions
          href: "#wp-key-solutions"
          data_ga_name: key-solutions
          data_ga_location: side-navigation
        - text: Customer case studies
          href: "#wp-customer-case-studies"
          data_ga_name: customer-case-studies
          data_ga_location: side-navigation
      - text: ROI calculator
        href: "#wp-roi-calculator"
        data_ga_name: roi-calculator
        data_ga_location: side-navigation
      - text: Premium features
        href: "#wp-premium-features"
        data_ga_name: premium-features
        data_ga_location: side-navigation
        nodes:
        - text: Faster code reviews
          href: "#wp-faster-code-reviews"
          data_ga_name: faster-code-reviews
          data_ga_location: side-navigation
        - text: Advanced ci/cd
          href: "#wp-advanced-ci-cd"
          data_ga_name: advanced-ci-cd
          data_ga_location: side-navigation
        - text: Enterprise agile planning
          href: "#wp-enterprise-agile-planning"
          data_ga_name: nterprise-agile-planning
          data_ga_location: side-navigation
        - text: Release controls
          href: "#wp-release-controls"
          data_ga_name: release-controls
          data_ga_location: side-navigation
        - text: Self managed reliability
          href: "#wp-self-managed-reliability"
          data_ga_name: self-managed-reliability
          data_ga_location: side-navigation
        - text: Other premium features
          href: "#wp-other-premium-features"
          data_ga_name: other-premium-features
          data_ga_location: side-navigation
      - text: Get in touch
        href: "#get-in-touch"
        data_ga_name: get-in-touch
        data_ga_location: side-navigation
    hyperlinks:
      text: Next Steps
      data:
      - text: Buy Premium now
        href: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
        variant: primary
        icon: false
        data_ga_name: buy-premium
        data_ga_location: side-navigation
      - text: Learn about Ultimate
        href: /pricing/ultimate/
        variant: secondary
        icon: true
        data_ga_name: learn-about-ultimate
        data_ga_location: side-navigation
  components:
  - name: plan-summary
    data:
      id: wp-summary
      title: Why Premium?
      subtitle: GitLab Premium is ideal for scaling organizations and for multi team usage.
      text: Available in both SaaS and self-managed deployment options, GitLab Premium helps to enhance team productivity and collaboration through **faster code reviews, advanced CI/CD, enterprise agile planning and release controls**. GitLab Premium adds enterprise level features like priority support, live upgrade assistance and a technical account manager (for eligible accounts). It also adds enterprise readiness features like High Availability, Disaster Recovery for self-managed instances.
      buttons:
        - variant: primary
          icon: false
          text: Buy Premium now
          url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
          data_ga_name: buy-premium-now
          data_ga_location: body
        - variant: secondary
          icon: true
          text: Learn about Ultimate
          url: /pricing/ultimate/
          data_ga_name: learn-about-ultimate
          data_ga_location: body
  - name: guest-calculator
    data:
      id: wp-guest-calculator
      title: Calculate the cost for your organization
      input_title: GitLab offers unlimited free guest users on Ultimate plans
      caption: |
        *All plans billed annually. The listed prices may be subject to applicable local and withholding taxes. Pricing may vary when purchased through a partner or reseller. See our [Pricing Page](/pricing/){data-ga-name="pricing" data-ga-location="guest calculator"} for more details.
      seats:
        title: Number of GitLab seats
        tooltip: GitLab seats are people with reporter, developer, maintainer or owner access. Guest users do not consume a seat in GitLab Ultimate. Learn more about seat usage <a href="https://docs.gitlab.com/ee/subscriptions/gitlab_com/#how-seat-usage-is-determined" data-ga-name="seat usage documentation" data-ga-location="user calculator tooltip">here</a>.
      guests:
        title: Number of guest users
        tooltip: Guest users can create and assign issues, view certain analytics, optionally view code, and more. Learn more <a href="https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions" data-ga-name="project members documentation" data-ga-location="user calculator tooltip">here</a>.
      premium:
        title: Premium monthly cost*
        link: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
        data_ga_name: buy premium
        data_ga_location: guest calculator
        button: Buy Premium
      ultimate:
        title: Ultimate monthly cost*
        link: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
        data_ga_name: buy ultimate
        data_ga_location: guest calculator
        button: Buy Ultimate
  - name: no-image-accordion
    data:
      id: wp-key-solutions
      title: GitLab Premium helps you
      is_accordion: true
      items:
        - icon:
            name: increase
            alt: Increase icon
            variant: marketing
          header: Increase Operational Efficiencies
          text: GitLab Premium introduces capabilities that allow enterprises analyze team, project and group trends to uncover patterns and setup consistent standards to improve overall productivity.
        - icon:
            name: speed-alt
            variant: marketing
            alt: Speed Icon
          header: Deliver Better Products Faster
          text: With Advanced CI/CD and faster code reviews, GitLab Premium helps you build, maintain, deploy and monitor complex application pipelines better to deliver products faster.
        - icon:
            name: lock-alt-5
            alt: Lock icon
            variant: marketing
          header: Reduce Security and Compliance Risk
          text: Release controls in GitLab Premium ensure that teams ship high quality and secure code.
  - name: case-study-carousel
    data:
      id: wp-customer-case-studies
      header: See how companies use GitLab Premium
      header_link:
        url: /customers/
        text: Read all case studies
        data_ga_name: Read all case studies
        data_ga_location: body
      case_studies:
        - title: NVIDIA
          subtitle: Learn how GitLab Geo supports NVIDIA’s innovation
          image:
            url: /nuxt-images/blogimages/nvidia.jpg
            alt:
          button:
            href: /customers/nvidia/
            text: Learn more
            data_ga_name: visit Nvidia case study
            data_ga_location: body
        - title: Fujitsu
          subtitle: Fujitsu Cloud Technologies improves deployment velocity and cross-functional workflows with GitLab
          image:
            url: /nuxt-images/blogimages/fjct_cover.jpg
            alt:
          button:
            href: /customers/fujitsu/
            text: Learn more
            data_ga_name: visit Fujitsu case study
            data_ga_location: body
        - title: Keytrade Bank
          subtitle: Keytrade Bank centralizes its tooling around GitLab
          image:
            url: /nuxt-images/blogimages/keytradebank_cover.jpg
            alt:
          button:
            href: /customers/keytradebank/
            text: Learn more
            data_ga_name: visit Keytrade Bank case study
            data_ga_location: body
        - title: Axway
          subtitle: Axway aims for elite DevOps status with GitLab
          image:
            url: /nuxt-images/blogimages/axway_casestudy_2.png
            alt:
          button:
            href: /customers/axway-devops/
            text: Learn more
            data_ga_name: visit Axway case study
            data_ga_location: body
        - title: Moneyfarm
          subtitle: Moneyfarm deploys faster using fewer tools with GitLab
          image:
            url: /nuxt-images/blogimages/moneyfarm_cover_image_july.jpg
            alt:
          button:
            href: /customers/moneyfarm/
            text: Learn more
            data_ga_name: visit Moneyfarm case study
            data_ga_location: body
        - title: EAB
          subtitle: Increasing development speed to keep students in school
          image:
            url: /nuxt-images/blogimages/eab_case_study_Image.jpg
            alt:
          button:
            href: /customers/EAB/
            text: Learn more
            data_ga_name: visit EAB case study
            data_ga_location: body
  - name: roi-calculator-block
    data:
      id: wp-roi-calculator
      header:
        gradient_line: true
        title:
          text: ROI calculator
          anchor: wp-roi-calculator
        subtitle: How much is your toolchain costing you?
      calc_data_src: calculator/roi/index
  features_block:
    id: wp-premium-features
    tier: premium
    header:
      gradient_line: true
      note: Please note this is not a comprehensive set of capabilities in GitLab Ultimate, visit [about.gitlab.com/features](https://about.gitlab.com/features){data-ga-name="features page" data-ga-location="body"} for the latest. GitLab continuously adds features every month and evaluates features that can be moved to lower tiers to benefit more users.
      title:
        text: Premium features
        anchor: wp-premium-features
        button:
          text: Compare all features
          data_ga_name: Compare all features
          data_ga_location: body
          url: /pricing/feature-comparison/
    pricing_themes:
      - id: wp-faster-code-reviews
        theme: Faster code reviews
        text: ensure high code quality across teams through seamless code review workflows.
        link:
          text: Learn more
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: /solutions/code-reviews/
      - id: wp-advanced-ci-cd
        theme: Advanced CI/CD
        text: allows you to build, maintain, deploy, and monitor complex pipelines.
        link:
          text: Learn more
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: /solutions/continuous-integration/
      - id: wp-enterprise-agile-planning
        theme: Enterprise agile planning
        text: helps you plan and manage your projects, programs, and products with integrated Agile support.
        link:
          text: Learn more
          data_ga_name: Agile Planning learn more
          data_ga_location: body
          url: /solutions/agile-delivery/
      - id: wp-release-controls
        theme: Release controls
        text: ensure teams ship high quality and secure code.
        link:
          text: Learn more
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/
      - id: wp-self-managed-reliability
        theme: Self-managed reliability
        text: ensures disaster recovery, high availability and load balancing of your self-managed deployment.
        link:
          text: Learn more
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: https://docs.gitlab.com/ee/administration/reference_architectures/#traffic-load-balancer
      - id: wp-other-premium-features
        text: Other Premium features
