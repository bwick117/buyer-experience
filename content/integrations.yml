---
  title: Integrate with GitLab
  description: You can integrate GitLab with external services for enhanced functionality.
  hero_block:
    title: Integrate with GitLab
    subtitle: You can integrate GitLab with external services for enhanced functionality.
    aos_animation: fade-down
    aos_duration: 500
    img_animation: zoom-out-left
    img_animation_duration: 1600
    primary_btn:
      text: Start your free trial
      url: /free-trial/
      data_ga_name: free trial
      data_ga_location: hero
    image:
      image_url: /nuxt-images/integrations/hero-image.jpg
      bordered: true
      alt: ""
  side_navigation_variant:
    links:
    - title: Services
      href: "#services"
    - title: Issue Trackers
      href: "#issue-trackers"
    - title: Authentication Sources
      href: "#authentication-sources"
    - title: Security
      href: "#security"
    - title: Continous Integration
      href: "#continous-integration"
    - title: Feature Enhancements
      href: "#feature-enhancements"
  integrations_blocks:
  - title: Services
    description: Integrate GitLab with services
    items:
    - name: Campfire
      description: Connect to chat with Campfire.
      url: https://docs.gitlab.com/ee/api/integrations.html#campfire
    - name: Jira
      description: Use Jira as the issue tracker.
      url: https://docs.gitlab.com/ee/integration/jira/
    - name: Pivotal Tracker
      description: Add commit messages to Pivotal Tracker stories.
      url: https://docs.gitlab.com/ee/user/project/integrations/pivotal_tracker.html
    - name: Slack Notifications
      description: Send notifications about project events to Slack.
      url: https://docs.gitlab.com/ee/user/project/integrations/slack.html
    call_to_action:
        text: Learn more about all 41 service integrations
        url: https://docs.gitlab.com/ee/user/project/integrations/index.html

  - title: Issue Trackers
    description: Integrate GitLab with issue trackers
    items:
    - name: Bugzilla
      url: https://docs.gitlab.com/ee/user/project/integrations/bugzilla.html
    - name: ClickUp
      url: https://docs.gitlab.com/ee/user/project/integrations/clickup.html
    - name: Custom Issue Tracker
      url: https://docs.gitlab.com/ee/user/project/integrations/custom_issue_tracker.html
    - name: EWM
      url: https://docs.gitlab.com/ee/api/integrations.html#engineering-workflow-management-ewm
    - name: Jira
      url: https://docs.gitlab.com/ee/integration/jira/
    - name: Redmine
      url: https://docs.gitlab.com/ee/user/project/integrations/redmine.html
    - name: YouTrack
      url: https://docs.gitlab.com/ee/user/project/integrations/youtrack.html
    - name: ZenTao
      url: https://docs.gitlab.com/ee/user/project/integrations/zentao.html
    call_to_action:
      text: Learn more about issue tracker integrations
      url: https://docs.gitlab.com/ee/integration/external-issue-tracker.html

  - title: Authentication Sources
    description: Integrate GitLab with authentication sources
    items:
    - name: Auth0 OmniAuth
      description: Enable the Auth0 OmniAuth provider
      url: https://docs.gitlab.com/ee/integration/auth0.html
    - name: BitBucket
      description: Enable sign-in with Bitbucket accounts
      url: https://docs.gitlab.com/ee/integration/bitbucket.html
    - name: Kerberos
      description: Authenticate with Kerberos
      url: https://docs.gitlab.com/ee/integration/kerberos.html
    - name: LDAP
      description: Enable sign-in with LDAP
      url: https://docs.gitlab.com/ee/integration/ldap.html
    - name: OmniAuth
      description: 'Enable sign-in through: Azure, Bitbucket, Crowd, Facebook, GitHub, GitLab.com, Google, SAML, Twitter'
      url: https://docs.gitlab.com/ee/integration/omniauth.html
    - name: OpenID Connect
      description: Use GitLab as an OpenID Connect identity provider
      url: https://docs.gitlab.com/ee/integration/openid_connect_provider.html
    - name: Vault
      description: Authenticate with Vault through GitLab OpenID Connect
      url: https://docs.gitlab.com/ee/integration/vault.html
    - name: SAML 2.0
      description: Configure GitLab as a SAML 2.0 Service Provider
      url: https://docs.gitlab.com/ee/integration/saml.html

  - title: Security
    sections:
    - title: Security Enhancements
      description: Integrate GitLab with security enhancements
      items:
      - name: Akismet
        description: Reduce spam with Akismet
        url: https://docs.gitlab.com/ee/integration/akismet.html
      - name: reCAPTCHA
        description: Verify new users with Google reCAPTCHA
        url: https://docs.gitlab.com/ee/integration/recaptcha.html
      subtitle: GitLab also provides features to improve the security of your own application. For more details, see [Secure your application](https://docs.gitlab.com/ee/user/application_security/index.html).

    - title: Security Partners
      description: Integrate GitLab with the security partners
      items:
      - name: Anchore
        url: https://docs.anchore.com/current/docs/configuration/integration/ci_cd/gitlab/
      - name: Bridgecrew
        url: https://docs.bridgecrew.io/docs/integrate-with-gitlab-self-managed
      - name: Checkmarx
        url: https://checkmarx.atlassian.net/wiki/spaces/SD/pages/1929937052/GitLab+Integration
      - name: Deepfactor
        url: https://www.deepfactor.io/docs/integrate-deepfactor-scanner-in-your-ci-cd-pipelines/#gitlab
      - name: GammaTech
        url: https://www.grammatech.com/codesonar-gitlab-integration
      - name: Indeni
        url: https://docs.cloudrail.app/#/integrations/gitlab
      - name: JScrambler
        url: https://docs.jscrambler.com/code-integrity/documentation/gitlab-ci-integration
      - name: Mend
        url: https://www.mend.io/gitlab/
      - name: Semgrep
        url: https://semgrep.dev/for/gitlab
      - name: StackHawk
        url: https://docs.stackhawk.com/continuous-integration/gitlab.html
      - name: Tenable
        url: https://docs.tenable.com/tenableio/Content/ContainerSecurity/GetStarted.htm
      - name: Venafi
        url: https://marketplace.venafi.com/xchange/620d2d6ed419fb06a5c5bd36/solution/6292c2ef7550f2ee553cf223
      - name: Veracode
        url: https://community.veracode.com/s/knowledgeitem/gitlab-ci-MCEKSYPRWL35BRTGOVI55SK5RI4A
      - name: Fortify
        url: https://www.microfocus.com/en-us/fortify-integrations/gitlab

  - title: Continous Integration
    items:
      - name: Jenkins
        description: Jenkins CI
        url: https://docs.gitlab.com/ee/integration/jenkins.html
      - name: Datadog
        description: Datadog to monitor for CI/CD job failures and performance issues
        url: https://docs.gitlab.com/ee/integration/datadog.html

  - title: Feature Enhancements
    items:
      - name: Gmail Actions Button
        text: Add GitLab actions to [Gmail actions buttons](https://docs.gitlab.com/ee/integration/gmail_action_buttons_for_gitlab.html)
      - name: Diagrams
        text: Configure [PlantUML](https://docs.gitlab.com/ee/administration/integration/plantuml.html) or [Kroki](https://docs.gitlab.com/ee/administration/integration/kroki.html) to use diagrams in AsciiDoc and Markdown documents.
      - name: Trello Cards
        text: Attach merge requests to [Trello](https://docs.gitlab.com/ee/integration/trello_power_up.html) card
      - name: Integrated Code Intelligence
        text: Enable integrated code intelligence powered by [Sourcegraph](https://docs.gitlab.com/ee/integration/sourcegraph.html)
      - name: Advanced Search
        text: Enable [Elasticsearch](https://docs.gitlab.com/ee/integration/elasticsearch.html) for [advanced search](https://docs.gitlab.com/ee/user/search/advanced_search.html)
