---
title: US Federal Support
description: US Federal Support
support-hero:
  data:
    title: US Federal Support
    content: |
      US Federal Support is built for companies and organizations that require that only US Citizens have access to the data contained within their support issues. 
      The unique requirements of US Federal Support result in the following specific policies:
side_menu:
  anchors:
    text: 'On this page'
    data:
      - text: 'Limitations'
        href: '#limitations'                             
      - text: "Hours of operation"
        href: "#hours-of-operation"
      - text: "US Federal Emergency support"
        href: "#us-federal-emergency-support"
      - text: "CCs are disabled"
        href: "#ccs-are-disabled"
      - text: "Discovery Session Requests"
        href: "#federal-discovery-sessions" 
  hyperlinks:
    text: ''
    data: []
components:
- name: support-copy-markdown
  data:
    block:
    - subtitle:
        text: Limitations
        id: limitations
      text: |
        To be eligible to utilize this instance, your account manager must approve and you must meet the following criteria:
          
          1. Be entitled to [premium or ultimate **self-managed** subscriptions](https://docs.gitlab.com/ee/subscriptions/self_managed/).
          2. Must be designated as a federal organization or federal systems integrator.

        For more information about utilizing this method of support, please contact your Account Manager.
    - subtitle:
        text: Hours of operation
        id: hours-of-operation
      text: |
        Due to the requirement that support be provided only by US citizens, US Federal Support hours of operation are limited to:
        - Monday through Friday, 0500-1700 Pacific Time
        - Weekday [Upgrade Assistance](/support/scheduling-upgrade-assistance/){data-ga-name="upgrade assistance" data-ga-location="body"} requests for the US Federal Support team may be scheduled within the US Federal hours of operation listed above. All [information](/support/scheduling-upgrade-assistance/#what-information-do-i-need-to-schedule-upgrade-assistance){data-ga-name="upgrade assistance information" data-ga-location="body"} relating to the request must be provided at least 1 week in advance.
    - subtitle:
        text: US Federal Emergency support 
        id: us-federal-emergency-support
      text: |
        The US Federal instance offers emergency support. However, this is limited to the hours of 0500 to 1700 Pacific Time 7 days a week.
        Emergencies can be filed either via the email address you should have received from your Account Manager or via the [Emergency support request form](https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001421112).
    - subtitle:
        text: CCs are disabled
        id: ccs-are-disabled
      text:
        To help ensure that non-US citizens are not inadvertently included in a support interaction, the "CC" feature is disabled. As a result, support personnel will be unable to add additional contacts to support tickets.
        
        Authorized support users may opt to include other contacts in support cases through the setup of a [shared organization](/support/managing-support-contacts/#shared-organizations) within the US Federal support portal. 
    - subtitle:
        text: Discovery Session Requests
        id: federal-discovery-sessions
      text:
        The federal support team provides users an opportunity to request a discovery session at the start of a new technical support case by selecting the "Discovery Session Request" option.

        Discovery requests are granted at the discretion of the assigned support engineer.

        The session is a brief opportunity to augment information provided when initiating a case and to work with a support engineer to collect relevant information such as screenshots, log files, replication steps, etc.

        **The process**

        When a discovery session request is made, the assigned engineer will review the case description and provide a link to schedule a session for a future date. The engineer may also request additional information asynchronously prior to sending a link to schedule the call if sufficient information hasn't yet been provided. When a session is scheduled but has not yet occurred troubleshooting can continue asynchronously via the case. In the event a resolution is found the engineer may opt to cancel the discovery session. After the scheduled session has occured the engineer will follow-up in the case with a synopsis of what was discussed and prompt to have the items collected uploaded to the case for further review.
        Troubleshooting will typically be done asynchronously from this point forward.

        **When is a discovery session not appropriate?**

        - In the event of a [Severity 1 incident](https://about.gitlab.com/support/definitions/#severity-1) the discovery session request should not be used. Instead you must follow the process to [file an emergency request](https://about.gitlab.com/support/us-federal-support/#us-federal-emergency-support).

        - Walkthroughs of documentation and engineer ride-along while attempting to setup a feature is not within the scope of a discovery session and may be declined in favor of asynchronous troubleshooting.

