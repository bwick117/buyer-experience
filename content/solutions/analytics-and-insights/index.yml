---
  title: Analytics and insights
  description: Analyze, discover and optimize the hidden value within your DevSecOps lifecycle
  image_alt: Analytics and insights
  template: 'industry'
  next_step_alt: true
  no_gradient: true
  components:
    - name: 'solutions-hero'
      data:
        title: Analytics & Insights
        subtitle: Analyze, discover and optimize the hidden value within your DevSecOps lifecycle
        aos_animation: fade-down
        aos_duration: 500
        title_variant: 'display1'
        mobile_title_variant: 'heading1-bold'
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com/solutions/analytics-and-insights
          text: Start your free trial
          data_ga_name: Start your free trial
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/analytics-and-insights/gitlab-video-thumbnail.png
          alt: "Video: gitlab for analytics and insights"
          bordered: true
          is_video_thumbnail: true
        modal_src: "https://player.vimeo.com/video/819308062?h=752d064728&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
    - name: copy
      data:
        block:
          - column_size: 8
            hide_horizontal_rule: true
            margin_bottom: 32
            subtitle: |
              Many teams struggle to track and evaluate the right metrics, optimize processes, and deliver software faster and predictably. With GitLab, the comprehensive AI-powered DevSecOps platform with a unified data store, teams get the end-to-end visibility and data they need to improve DevSecOps processes across the software development lifecycle.
            subtitle_mobile_padding: true 
    - name: 'side-navigation-variant'
      slot_enabled: true
      links:
        - title: Benefits
          href: '#benefits'
        - title: Capabilities
          href: '#capabilities'
        - title: Customers
          href: '#customers'
        - title: Recognitions
          href: '#recognitions'
        - title: Resources
          href: '#resources'
      slot_content:
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'analytics-insights-featured'
              data:
                title: |
                  Provide more context to technology delivery and process improvements through analytics and dashboards
                cards:
                  - title:  Improve DevSecOps processes across the software development lifecycle
                    rows:
                      - icon:
                          name: graph
                        title: Analyze your modern software factory
                        text: Identify and track KPIs and aggregate analytics to drive visibility and improvements.
                      - icon:
                          name: target
                        title: Optimize planning efforts with metrics
                        text: |
                          Utilize historical data to forecast trends and project future behavior of value streams.
                      - icon:
                          name: growth
                        title: Build continuously improving DevSecOps processes
                        text: |
                          Uncover gaps and optimize processes with unified dashboards across Dev, Sec and Ops.
        - name: 'div'
          id: capabilities
          isDarkMode: true
          slot_enabled: true
          slot_content:
            - name: 'analytics-insights-showcase-faq'
              data:
                dark_bg: true
                content_items:
                  - title: Software development lifecycle optimization
                    id: opti
                    dropdownItems:
                      - title: Executive Insights
                        video:
                          url: "https://player.vimeo.com/video/819308062?h=752d064728&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
                          thumbnail: /nuxt-images/solutions/analytics-and-insights/vsd-thumbnail.png
                        text: | 
                          - The [value streams dashboard](https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html) drives technology transformation with a unified view of software value delivery metrics.
                          - [GitLab Duo’s](https://docs.gitlab.com/ee/user/ai_features.html#show-deployment-frequency-forecast) value stream forecasting capability takes historical data and uses data trends across your development lifecycle to predict the future behavior of your value stream metrics.
                          - [DevOps adoption](https://docs.gitlab.com/ee/user/group/devops_adoption/) analytics drives DevOps transformations by unearthing adoption barriers for your teams.
                      - title: Security Insights
                        video:
                          url: https://www.youtube.com/embed/QHQHN4luNpc?enablejsapi=1
                          thumbnail: /nuxt-images/solutions/analytics-and-insights/security-dashboard-thumbnail.png
                        text: |
                          - [Security Dashboards](https://docs.gitlab.com/ee/user/application_security/security_dashboard/) and Security Center highlight trends on vulnerabilities detected by security scanners.
                          - [Project vulnerability grades](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#project-vulnerability-grades) track trends across projects for quick assessment of at-risk projects.
                          - [The Vulnerability Report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/index.html#operational-vulnerabilities) helps manage, triage, and remediate operational vulnerabilities found in your running applications.
                          - [Compliance reports](https://docs.gitlab.com/ee/user/compliance/compliance_report/index.html) provide a consolidated view of compliance signals such as segregation of duties, framework compliance, license compliance, user activity, and merge request/pipeline results.
                      - title: Operational Insights
                        video:
                          url: https://www.youtube.com/embed/Uqz0qHmeZxo?enablejsapi=1
                          thumbnail: /nuxt-images/solutions/analytics-and-insights/environment-dashboard-thumbnail.png
                        text: |
                          - The [Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/) provides a summary of each project’s operational health, including pipeline and alert status.
                          - [CI/CD Analytics](https://docs.gitlab.com/ee/user/analytics/ci_cd_analytics.html) provides pipeline success rate analysis and data to optimize pipeline performance.
                          - The [Environments Dashboard](https://docs.gitlab.com/ee/ci/environments/environments_dashboard.html) provides a cross-project, environment-based view, to track the progress as changes flow from development to staging, and then to production.              
                      - title: Team Insights
                        video:
                          url: https://www.youtube.com/embed/lM_FbVYuN8s?enablejsapi=1
                          thumbnail: "/nuxt-images/solutions/analytics-and-insights/dora-metrics-thumbnail.png"
                        text: |
                          - [Planning insights](https://docs.gitlab.com/ee/user/analytics/issue_analytics.html) via issue analytics, burndown and burnup charts, and roadmap analysis helps streamline development workflow.
                          - [Visualize team contribution data](https://docs.gitlab.com/ee/user/group/contribution_analytics/) over different time periods with contribution analytics.
                          - [Productivity insights](https://docs.gitlab.com/ee/user/group/value_stream_analytics/index.html) helps unblock teams by analyzing development velocity, merge request analytics, commit analytics, and code review analytics.
                          - [Value stream analytics](https://docs.gitlab.com/ee/user/group/value_stream_analytics/index.html) increase the effectiveness and efficiency of development teams by highlighting waste and surfacing actionable insights for improvement.
                          - [DORA4 metrics](https://about.gitlab.com/solutions/value-stream-management/dora/) help benchmark DevSecOps maturity and identify areas for process improvement.
        - name: 'div'
          id: 'customers'
          slot_enabled: true
          slot_content:
            - name: 'analytics-insights-customer-success-card'
              data:
                header: Customer success
                customers_cta: true
                cta:
                  href: '/customers/'
                  text: View all customer stories
                  data_ga_name: customers
                case_study:
                  logo_url: /nuxt-images/logos/zoopla-logo.png
                  institution_name: Zoopla
                  content:
                    case_url: /customers/zoopla/
                    img_url: /nuxt-images/blogimages/zoopla_cover_image.jpg
                    img_alt: A number of houses seen from above
                    text: How Zoopla deploys 700% faster with GitLab
        - name: 'div'
          id: 'recognitions'
          slot_enabled: true
          slot_content:
            - name: 'report-cta'
              data:
                title: Analyst reports
                bg_dark: true
                reports:
                - description: "GitLab recognized as the only Leader in The Forrester Wave™: Integrated Software Delivery Platforms, Q2 2023"
                  url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
                - description: "GitLab recognized as a Leader in the 2023 Gartner® Magic Quadrant™ for DevOps Platforms"
                  url: /gartner-magic-quadrant/
        - name: 'solutions-resource-carousel'
          slot_enabled: true
          id: resources
          data:
            column_size: 4
            title: Related resources
            cards:
                  - icon:
                      name: video
                      variant: marketing
                      alt: Video Icon
                    event_type: "Video"
                    header: Security Dashboard - Advanced Security Testing
                    link_text: "Watch now"
                    image: "/nuxt-images/solutions/analytics-and-insights/security-dashboard-thumbnail.png"
                    href: https://www.youtube.com/embed/QHQHN4luNpc?enablejsapi=1
                    data_ga_name: Security Dashboard - Advanced Security Testing
                    data_ga_location: resource cards
                  - icon:
                      name: video
                      variant: marketing
                      alt: Video Icon
                    event_type: "Video"
                    header: Vulnerability Management - Advanced Security Testing
                    link_text: "Watch now"
                    image: "/nuxt-images/solutions/analytics-and-insights/vulnerability-management-thumbnail.png"
                    href: https://www.youtube.com/embed/8SJHz6BCgXM?enablejsapi=1
                    data_ga_name: Vulnerability Management - Advanced Security Testing
                    data_ga_location: resource cards
                  - icon:
                      name: video
                      variant: marketing
                      alt: Video Icon
                    event_type: "Video"
                    header: DORA metrics - User Analytics
                    link_text: "Watch now"
                    image: "/nuxt-images/solutions/analytics-and-insights/dora-metrics-thumbnail.png"
                    href: https://www.youtube.com/embed/lM_FbVYuN8s?enablejsapi=1
                    data_ga_name: DORA metrics - User Analytics
                    data_ga_location: resource cards
                  - icon:
                      name: video
                      variant: marketing
                      alt: Video Icon
                    event_type: "Video"
                    header: Group Code Coverage Analytics - Ops Insights
                    link_text: "Watch now"
                    image: "/nuxt-images/solutions/analytics-and-insights/code-group-coverage-data-thumbnail.png"
                    href: https://www.youtube.com/embed/udhJJDa11Fk?enablejsapi=1
                    data_ga_name: Group Code Coverage Analytics - Ops Insights
                    data_ga_location: resource cards
                  - icon:
                      name: video
                      variant: marketing
                      alt: Video Icon
                    event_type: "Video"
                    header: Code Review Analytics - Faster Code Review
                    link_text: "Watch now"
                    image: "/nuxt-images/solutions/analytics-and-insights/code-review-analytics-thumbnail.png"
                    href: https://www.youtube.com/embed/OkLaWhYkASM?enablejsapi=1
                    data_ga_name: Code Review Analytics - Faster Code Review
                    data_ga_location: resource cards
                  - icon:
                      name: blog
                      variant: marketing
                      alt: Blog Icon
                    event_type: "Blog"
                    header: "How GitLab's application security dashboard helps AppSec engineers"
                    link_text: "Learn more"
                    href: https://about.gitlab.com/blog/2020/07/07/secure-stage-for-appsec/
                    image: /nuxt-images/blogimages/ralph-kayden-unsplash.jpg
                    data_ga_name: "How GitLab's application security dashboard helps AppSec engineers"
                    data_ga_location: resource cards
                  - icon:
                      name: blog
                      variant: marketing
                      alt: Blog Icon
                    event_type: "Blog"
                    header: "Value stream management: Total Time Chart simplifies top-down optimization flow"
                    link_text: "Learn more"
                    href: https://about.gitlab.com/blog/2023/06/01/value-stream-total-time-chart/
                    image: /nuxt-images/blogimages/clocks.jpg
                    data_ga_name: "Value stream management: Total Time Chart simplifies top-down optimization flow"
                    data_ga_location: resource cards
                  
                  - icon:
                      name: blog
                      variant: marketing
                      alt: Blog Icon
                    event_type: "Blog"
                    header: "The role of Value Stream Analytics in GitLab's DevOps Platform"
                    link_text: "Learn more"
                    href: https://about.gitlab.com/blog/2022/01/24/gitlab-value-stream-analytics/
                    image: /nuxt-images/blogimages/Understand-Highly-Technical-Spaces.jpg
                    data_ga_name: "The role of Value Stream Analytics in GitLab's DevOps Platform"
                    data_ga_location: resource cards

                  - icon:
                      name: blog
                      variant: marketing
                      alt: Blog Icon
                    event_type: "Blog"
                    header: "Troubleshoot delays with our Code Review Analytics tool"
                    link_text: "Learn more"
                    href: https://about.gitlab.com/blog/2020/03/18/troubleshoot-delays-with-code-review-analytics/
                    image: /nuxt-images/blogimages/code_review_analytics.png
                    data_ga_name: "Troubleshoot delays with our Code Review Analytics tool"
                    data_ga_location: resource cards
                  - icon:
                      name: blog
                      variant: marketing
                      alt: Blog Icon
                    event_type: "Blog"
                    header: "Understand how your teams adopt DevOps with DevOps reports"
                    link_text: "Learn more"
                    href: https://about.gitlab.com/blog/2021/12/15/devops-adoption/
                    image: /nuxt-images/blogimages/john-schnobrich-FlPc9_VocJ4-unsplash.jpg
                    data_ga_name: "Understand how your teams adopt DevOps with DevOps reports"
                    data_ga_location: resource cards
                  - icon:
                      name: blog
                      variant: marketing
                      alt: Blog Icon
                    event_type: "Blog"
                    header: "Product Analytics: A sneak peek at our upcoming feature"
                    link_text: "Learn more"
                    href: https://about.gitlab.com/blog/2023/03/27/introducing-product-analytics-in-gitlab/
                    image: /nuxt-images/blogimages/blog-compliance.jpg
                    data_ga_name: "Product Analytics: A sneak peek at our upcoming feature"
                    data_ga_location: resource cards
