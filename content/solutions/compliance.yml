---
  title: Continuous Software Compliance with GitLab
  description: How to use GitLab to build compliant applications with a secure software supply chain.
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  Automate compliance, reduce risks
        title: Software Compliance with GitLab
        subtitle: Build applications that meet common regulatory standards with a secure software supply chain.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Try Ultimate for Free
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Learn about pricing
          url: /pricing/
          data_ga_name: pricing
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/compliance/compliance-hero.jpeg
          image_url_mobile: /nuxt-images/solutions/compliance/compliance-hero.jpeg
          alt: "Image: gitlab for public sector"
          bordered: true
    - name: 'by-industry-intro'
      data:
        intro_text: Trusted by
        logos:
          - name: Duncan Aviation Logo
            image: "/nuxt-images/logos/duncan-aviation-logo.svg"
            url: /customers/duncan-aviation/
            aria_label: Link to Duncan Aviation case study
          - name: Curve Logo
            image: /nuxt-images/logos/curve-logo.svg
            url: /customers/curve/
            aria_label: Link to Curve customer case study
          - name: Hilti Logo
            image: /nuxt-images/logos/hilti_logo.svg
            url: /customers/hilti/
            aria_label: Link to Hilti customer case study
          - name: The Zebra Logo
            image: /nuxt-images/logos/zebra.svg
            url: /customers/thezebra/
            aria_label: Link to The Zebra customer case study
          - name: New 10 Logo
            image: /nuxt-images/logos/new10-logo.svg
            url: /customers/new10/
            aria_label: Link to Conversica customer case study
          - name: Chorus Logo
            image: /nuxt-images/logos/chorus-logo.svg
            url: /customers/chorus/
            aria_label: Link to Bendigo and Adelaide Bank customer case study
    - name: 'side-navigation-variant'
      links:
        - title: Overview
          href: '#overview'
        - title: Capabilities
          href: '#capabilities'
        - title: Customers
          href: '#customers'
        - title: Pricing
          href: '#pricing'
        - title: Resources
          href: '#resources'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Simplify and automate software compliance
                image:
                  image_url: "/nuxt-images/solutions/compliance/compliance-overview.png"
                  alt: Continuous Software Compliance with GitLab
                is_accordion: false
                items:
                  - icon:
                      name: devsecops
                      alt: Devsecops Icon
                      variant: marketing
                    header: Manage risk
                    text: Go beyond simply reducing security flaws in the code
                  - icon:
                      name: clipboard-check
                      alt: Clipboard Check Icon
                      variant: marketing
                    header: Simple and frictionless
                    text: An integrated experience to define, enforce, and report on compliance
                  - icon:
                      name: release
                      alt: Shield Check Icon
                      variant: marketing
                    header: Implement guardrails
                    text: Control access and implement policies
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                markdown: true
                subtitle: Fast, secure, compliant.
                sub_description: ''
                white_bg: true
                sub_image: /nuxt-images/solutions/compliance/compliance-benefits.png
                solutions:
                  - title: Policy management
                    description: Define rules and policies to adhere to compliance frameworks and common controls
                    list:
                      - "**Granular user roles and permissions:** Define user roles and permission levels that make sense for your organization"
                      - "**Access control:** Limit access with two-factor authentication and expiration tokens"
                      - "**Compliance settings:** Define and enforce compliance policies for specific projects, groups, and users"
                      - "**Credentials inventory:** Keep track of all the credentials that can be used to access a GitLab self-managed instance"
                      - "**Protected branches:** Control unauthorized modifications to specific branches — including creating, pushing, and deleting a branch — without adequate permissions or approvals"
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html?_gl=1*1r05yn6*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NTY1NDE3OC4xNDguMS4xNjY1NjU1ODM3LjAuMC4w#policy-management
                    data_ga_name: policy mnagement
                    data_ga_location: solutions block
                  - title: Compliant workflow automation
                    description: Enforce defined rules, policies, and separation of duties while reducing overall business risk
                    list:
                      - "**Compliance framework project templates:** Create projects that map to specific audit protocols such as HIPAA to help maintain an audit trail and manage compliance programs"
                      - "**Compliance framework project labels:** Easily apply common compliance settings to a project with a label"
                      - "**Compliance framework pipelines:** Define compliance jobs that should be run in every pipeline to ensure that security scans are run, artifacts are created and stored, or any other steps required by your organizational requirements"
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html?_gl=1*nbfxzt*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NTY1NDE3OC4xNDguMS4xNjY1NjU2NDIyLjAuMC4w#compliant-workflow-automation
                    data_ga_name: workflow automation
                    data_ga_location: solutions block
                  - title: Audit management
                    description: Prepare for audits and better understand the root cause of issues with easy access to audit data
                    list:
                      - "**[Audit events:](https://docs.gitlab.com/ee/administration/audit_events.html)** Track important events such as changes to user permission levels, who added a new user, or who removed a user"
                      - "**[Streaming audit events:](https://docs.gitlab.com/ee/administration/audit_event_streaming.html)** Consolidate your audit logs in a tool of your choice"
                      - "**[Audit reports:](https://docs.gitlab.com/ee/administration/audit_reports.html)** Respond to auditors by generating comprehensive reports such as instance, group, and project events, impersonation data, sign-in, and user events"
                      - "**[Compliance report:](https://docs.gitlab.com/ee/user/compliance/compliance_report/)** Get a high-level view of compliance violations and the reasons and severity of violations in merge requests"
                  - title: Vulnerability and dependency management
                    description: View, triage, trend, track, and resolve vulnerabilities and dependencies in your applications
                    list:
                      - "**[Security dashboards:](https://docs.gitlab.com/ee/user/application_security/security_dashboard/)** Access current security status applications and initiate remediation"
                      - "**[Software bill of materials:](https://docs.gitlab.com/ee/user/application_security/dependency_list/)** Scan application and container dependencies for security flaws and create a software bill of materials (SBOM) of the dependencies used"
        - name: 'div'
          id: 'customers'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                align: left
                header: |
                  Trusted by enterprises.
                  <br />
                  Loved by developers.
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/duncan-aviation-logo.svg
                      alt: Duncan Aviation Logo
                    quote: GitLab helped us to automate manual processes using pipelines. Now we are deploying code regularly, getting essential changes and fixes to our customers a lot faster
                    author: Ben Ferguson
                    position: Senior Programmer, Duncan Aviation
                    ga_carousel: duncan aviation testimonial
                    url: /customers/duncan-aviation/
                  - title_img:
                      url: /nuxt-images/logos/curve-logo.svg
                      alt: Curve Logo
                    quote: Before we moved to GitLab there was a big burden on operation teams. It was a battle to enable developers to effectively do their jobs. The obvious choice was to have everything in one place and well-contained through a single pane of glass
                    author: Ed Shelto
                    position: Site Reliability Engineer, Curve
                    ga_carousel: curve testimonial
                    url: /customers/curve/
                  - title_img:
                      url: /nuxt-images/logos/hilti_logo.svg
                      alt: Hilti Logo
                    quote: GitLab is bundled together like a suite and then ships with a very sophisticated installer. And then it somehow works. This is very nice if you're a company which just wants to get it up and running.
                    author: Daniel Widerin
                    position: Head of Software Delivery, HILTI
                    ga_carousel: hilti testimonial
                    url: /customers/hilti/
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              data:
                header: Which tier is right for you?
                cta:
                  url: /pricing/
                  text: Learn more about pricing
                  data_ga_name: pricing
                  data_ga_location: free tier
                  aria_label: pricing
                tiers:
                  - id: free
                    title: Free
                    items:
                      - Static application security testing (SAST) and secrets detection
                      - Findings in json file
                    link:
                      href: /pricing/
                      text: Learn more
                      data_ga_name: pricing
                      data_ga_location: free tier
                      aria_label: free tier
                  - id: premium
                    title: Premium
                    items:
                      - Static application security testing (SAST) and secrets detection
                      - Findings in json file
                      - MR approvals and more common controls
                    link:
                      href: /pricing/
                      text: Learn more
                      data_ga_name: pricing
                      data_ga_location: premium tier
                      aria_label: premium tier
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Everything in Premium plus
                      - Comprehensive security scanners include SAST, DAST, Secrets, dependencies, containers, IaC, APIs, cluster images, and fuzz testing
                      - Actionable results within the MR pipeline
                      - Compliance pipelines
                      - Security and Compliance dashboards
                      - Much more
                    link:
                      href: /pricing/
                      text: Learn more
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: ultimate tier
                    cta:
                      href: /free-trial/
                      text: Try Ultimate for Free
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: ultimate tier
        - name: 'div'
          id: 'resources'
          slot_enabled: true
          slot_content:
          - name: solutions-resource-cards
            data:
              title: Related Resources
              align: left
              column_size: 4
              grouped: true
              cards:
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: Compliant pipelines
                  link_text: "Watch now"
                  image: "/nuxt-images/solutions/compliance/compliant-pipelines.jpeg"
                  href: https://www.youtube.com/embed/jKA_e_jimoI
                  data_ga_name: Compliant pipelines
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: Continuous software compliance
                  link_text: "Watch now"
                  image: "/nuxt-images/solutions/compliance/continuous-software-compliance.jpeg"
                  href: https://player.vimeo.com/video/694891993?h=7768f52e29
                  data_ga_name: Continuous software compliance
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: SBOM and Attestation
                  link_text: "Watch now"
                  image: "/nuxt-images/solutions/compliance/sbom-and-attestation.jpeg"
                  href: https://www.youtube.com/embed/wX6aTZfpJv0
                  data_ga_name: SBOM and Attestation
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Ebook Icon
                  event_type: "Book"
                  header: "Guide to software supply chain security"
                  link_text: "Learn more"
                  image: "/nuxt-images/blogimages/modernize-cicd.jpg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/360915/35d042c6-3449-4d50-b2e9-b08d9a68f7a1.pdf
                  data_ga_name: "Guide to software supply chain security"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Ebook Icon
                  event_type: "Book"
                  header: "GitLab DevSecOps survey"
                  link_text: "Learn more"
                  image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
                  href: https://cdn.pathfactory.com/assets/10519/contents/432983/c6140cad-446b-4a6c-96b6-8524fac60f7d.pdf
                  data_ga_name: "GitLab DevSecOps survey"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Blog Icon
                  event_type: "Blog"
                  header: "The importance of compliance in DevOps"
                  link_text: "Learn more"
                  href: https://about.gitlab.com/blog/2022/08/15/the-importance-of-compliance-in-devops/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name: "The importance of compliance in DevOps"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Blog Icon
                  event_type: "Blog"
                  header: "Top 5 compliance features to leverage in GitLab"
                  link_text: "Learn more"
                  href: https://about.gitlab.com/blog/2022/07/13/top-5-compliance-features-to-leverage-in-gitlab/
                  image: /nuxt-images/blogimages/cover_image_regenhu.jpg
                  data_ga_name: "Top 5 compliance features to leverage in GitLab"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    alt: Blog Icon
                    variant: marketing
                  event_type: "Blog"
                  header: "How to enforce separation of duties and achieve compliance"
                  link_text: "Learn more"
                  href: "https://about.gitlab.com/blog/2022/04/04/ensuring-compliance/"
                  image: /nuxt-images/blogimages/scm-ci-cr.png
                  data_ga_name: "How to enforce separation of duties and achieve compliance"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    alt: Blog Icon
                    variant: marketing
                  event_type: "Blog"
                  header: "What you need to know about DevOps Audits"
                  link_text: "Learn more"
                  href: "https://about.gitlab.com/blog/2022/08/31/what-you-need-to-know-about-devops-audits/"
                  image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
                  data_ga_name: "What you need to know about DevOps Audits"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: Report Icon
                    variant: marketing
                  event_type: "Analyst Report"
                  header: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  link_text: "Learn more"
                  href: "https://about.gitlab.com/analysts/gartner-ast22/"
                  image: /nuxt-images/blogimages/anwb_case_study_image_2.jpg
                  data_ga_name: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  data_ga_location: resource cards
    - name: solutions-cards
      data:
        title: Do more with GitLab
        column_size: 4
        link :
          url: /solutions/
          text: Explore more Solutions
          data_ga_name: solutions explore more
          data_ga_location: body
        cards:
          - title: DevSecOps
            description: GitLab empowers your teams to balance speed and security by automating software delivery and securing your end-to-end software supply chain.
            icon:
              name: devsecops
              alt: Devsecops Icon
              variant: marketing
            href: /solutions/security-compliance/
            data_ga_name: devsecpps learn more
            data_ga_location: body
          - title: Software Supply Chain Security
            description: Ensure your software supply chain is secure and compliant.
            icon:
              name: continuous-delivery
              alt: Continuous Delivery
              variant: marketing
            href: /solutions/supply-chain/
            data_ga_name: software supply chain security learn more
            data_ga_location: body
          - title: Automated software delivery
            description: Automation essentials for achieving digital innovation, cloud native transformations and application modernization
            icon:
              name: continuous-integration
              alt: Continuous Integration Icon
              variant: marketing
            href: /solutions/delivery-automation/
            data_ga_name: automated software delivery learn more
            data_ga_location: body

