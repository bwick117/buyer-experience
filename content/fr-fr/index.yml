---
  title: La plateforme DevSecOps
  description: De la planification à la production, faites collaborer les équipes dans une seule application. Livrez du code sécurisé de manière plus efficace pour offrir votre valeur ajoutée plus rapidement.
  schema_org: >
    {'@context':'https://schema.org','@type': 'Corporation','name':'GitLab','legalName':'GitLab Inc.','tickerSymbol':'GTLB','url':'https://about.gitlab.com','logo':'https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png', 'description' : 'GitLab est la plateforme DevOps qui permet aux entreprises d'optimiser le rendement global du développement logiciel en livrant des logiciels plus rapidement et plus efficacement, tout en renforçant la sécurité et la conformité. Grâce à GitLab, chaque équipe au sein de l'entreprise peut planifier, compiler, sécuriser et déployer des logiciels de manière collaborative et permettre ainsi à l'entreprise de prospérer plus rapidement, en toute transparence, en toute cohérence et avec une traçabilité complète. GitLab offre, au moyen d'une approche commerciale à noyau ouvert, une solution de bout en bout du cycle de vie du développement logiciel. GitLab compte 30 millions d'utilisateurs inscrits et plus d'un million d'utilisateurs actifs possédant une licence, et dispose d'une communauté dynamique de plus de 2 500 contributeurs. GitLab partage ouvertement plus d'informations que la plupart des entreprises, avec son approche publique par défaut. En d'autres termes, nos projets, notre stratégie, notre orientation et nos métriques sont discutés ouvertement et sont disponibles sur notre site Web. Nos valeurs centrales : la Collaboration, les Résultats, l'Efficacité, la Diversité, l'Inclusion et l'Appartenance, l'Itération et la Transparence (CRÉDIT). Celles-ci sont la base de notre culture.','foundingDate':'2011','founders':[{'@type':'Person','name':'Sid Sijbrandij'},{'@type':'Person','name':'Dmitriy Zaporozhets'}],'slogan': 'Notre mission est de faire passer tous les travaux créatifsdu mode lecture seule au mode lecture-écriture afin que tout le monde puissent contribuer.','address':{'@type':'PostalAddress','streetAddress':'268 Bush Street #350','addressLocality':'San Francisco','addressRegion':'CA','postalCode':'94104','addressCountry':'USA'},'awards': 'Meilleure équipe d'ingénieurs au classement Comparably 2021, Gartner Magic Quadrant 2021 dans le domaine des tests de sécurité des applications - Challenger, Prix DevOps Dozen du meilleur fournisseur de solutions DevOps pour 2019, Prix 451 Firestarter de 451 Research','knowsAbout': [{'@type': 'Thing','name': 'DevOps'},{'@type': 'Thing','name': 'CI/CD'},{'@type': 'Thing','name': 'DevSecOps'},{'@type': 'Thing','name': 'GitOps'},{'@type': 'Thing','name': 'DevOps Platform'}],'sameAs':['https://www.facebook.com/gitlab','https://twitter.com/gitlab','https://www.linkedin.com/company/gitlab-com','https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg']}
  hero_scroll_gallery:
    controls:
      prev: diapositive précédente
      next: diapositive suivante
    slides:
      - type: block
        title: Des logiciels. Plus rapidement.
        blurb: GitLab est la plateforme DevSecOps alimentée par l'IA la plus complète.
        primary_button:
          text: Bénéficier d'un essai gratuit
          link: 'https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial'
          data_ga_name: 'free trial'
          data_ga_location: 'hero'
      - type: card
        image:
          src: '/nuxt-images/home/hero/hero-1.png'
          alt: 'Image de marque des tableaux GitLab'
        blurb: Les leaders du secteur choisissent GitLab pour créer des logiciels critiques
        button:
          text: Pourquoi utiliser GitLab ?
          link: https://about.gitlab.com/why-gitlab/
          data_ga_name: why gitlab
          data_ga_location: hero
      - type: card
        theme: sombre
        image:
          src: '/nuxt-images/home/hero/hero-2.png'
          alt: 'Image de marque des tableaux GitLab'
        blurb: Les flux de travail alimentés par l'IA améliorent l'efficacité et réduisent les durées de cycle
        button:
          text: Découvrir GitLab Duo
          link: https://about.gitlab.com/gitlab-duo/
          data_ga_name: duo
          data_ga_location: hero
      - type: card
        image:
          src: '/nuxt-images/home/hero/hero-2.png'
          alt: 'Image de marque des tableaux GitLab'
        blurb: GitLab nommé leader des plateformes DevOps
        button:
          text: Lire le rapport
          link: https://about.gitlab.com/gartner-magic-quadrant/
          data_ga_name: gartner
          data_ga_location: hero
      - type: block
        title: Lancez-vous dès aujourd'hui
        blurb: Découvrez ce que votre équipe pourrait faire avec la plateforme DevSecOps alimentée par IA.
        primary_button:
          text: Bénéficier d'un essai gratuit
          link: 'https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial'
          data_ga_name: 'get started free trial'
          data_ga_location: 'hero'
        secondary_button:
          text: 'Contacter le service commercial'
          link: /sales
          data_ga_name: 'sales'
          data_ga_location: 'hero'
          icon:
            name: chevron-right
            variant: product
      - type: image
        src: '/nuxt-images/home/hero/value-stream-img.svg'
        alt: ''
  customer_logos_block:
    text:
      legend: Adoptée par
      url: '/customers/'
      data_ga_name: 'trusted by'
      data_ga_location: 'home case studies block'
    showcased_enterprises:
      - image_url: '/nuxt-images/home/logo_tmobile_mono.svg'
        link_label: Lien vers la page d'accueil du webcast de T-Mobile et GitLab'
        alt: 'Logo T-Mobile'
        url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
      - image_url: '/nuxt-images/home/logo_goldman_sachs_mono.svg'
        link_label: Lien vers l'étude de cas de Goldman Sachs
        alt: 'Logo Goldman Sachs'
        url: '/customers/goldman-sachs/'
      - image_url: '/nuxt-images/software-faster/airbus-logo.png'
        link_label: Lien vers l'étude de cas d'Airbus
        alt: 'Logo Airbus'
        url: '/customers/airbus/'
      - image_url: '/nuxt-images/logos/lockheed-martin.png'
        link_label: Lien vers l'étude de cas de Lockheed Martin
        alt: 'Logo Lockheed martin'
        url: /customers/lockheed-martin/
        size: lg
      - image_url: "/nuxt-images/logos/carfax-logo.png"
        link_label: Lien vers l'étude de cas de Carfax
        alt: "Carfax logo"
        url: /customers/carfax/
      - image_url: '/nuxt-images/home/logo_nvidia_mono.svg'
        link_label: Lien vers l'étude de cas de Nvidia
        alt: 'Logo Nvidia'
        url: /customers/nvidia/
      - image_url: '/nuxt-images/home/logo_ubs_mono.svg'
        link_label: Lien vers l'article de blog intitulé Comment UBS a créé sa propre plateforme DevSecOps en utilisant GitLab
        alt: 'Logo UBS'
        url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
  intro_block:
    title: Équilibrez vitesse et sécurité, sur une seule et même plateforme
    blurb: Automatisez la livraison de logiciels, augmentez la productivité et sécurisez votre chaîne logistique logicielle de bout en bout.
    primary_button:
      href: '/platform/'
      text: 'Découvrir la plateforme'
      data_ga_name: 'discover the platform'
      data_ga_location: 'body'
    secondary_button:
      href: '#'
      text: 'Qu''est-ce que GitLab ?'
      data_ga_name: 'what is gitlab'
      data_ga_location: 'body'
      modal:
          video_link: https://player.vimeo.com/video/799236905?h=4eee39a447&badge=0&autopause=0&player_id=0&app_id=58479
          video_title: Qu'est-ce que GitLab ?
    image:
      src: '/nuxt-images/home/hero/developer-productivity-img.svg'
      alt: 'Image de marque des tableaux GitLab'
  blurb_grid:
    - icon:
        name: devsecops
        size: md
        variant: marketing
      title: Simplifiez votre chaîne d'outils
      blurb: Tous les <a data-ga-name='platform' data-ga-location='body' href='/platform/'>outils DevSecOps essentiels</a> réunis en un seul et même endroit.
    - icon:
        name: agile-large
        size: md
        variant: marketing
      title: Accélérer la livraison de logiciels
      blurb: Automatisation, <a data-ga-name='gitlab-duo' data-ga-location='body' href='/gitlab-duo/'>flux de travail alimentés par l'IA</a> et plus encore.
    - icon:
        name: shield-check-large
        size: md
        variant: marketing
      title: Sécurité intégrée
      blurb: La sécurité est <a data-ga-name='security-compliance' data-ga-location='body' href='/solutions/security-compliance/'>intégrée, elle n'est pas ajoutée par la suite</a>.
    - icon:
        name: gitlab-cloud
        size: md
        variant: marketing
      title: Déployez n'importe où
      blurb: Dites adieu au <a data-ga-name='multicloud' data-ga-location='body' href='/topics/multicloud/ '>verrouillage des fournisseurs de services cloud</a>.
  feature_showcase:
    - logo:
        src: /nuxt-images/logos/lockheed-martin.png
        alt: ''
      icon:
        name: stopwatch
        size: md
        variant: marketing
      blurb: Découvrez comment <a data-ga-name='lockheed-martin' data-ga-location='body' href='/customers/lockheed-martin/'>Lockheed Martin</a> économise du temps, de l'argent et des efforts technologiques grâce à GitLab
      button:
        text: Lire le témoignage
        href: '/customers/lockheed-martin/'
        data_ga_name: read story lockheed martin
        data_ga_location: body
      stats:
        - value: 80x
          blurb: faster CI pipeline builds
          icon:
            name: arrow-up
            variant: product
            size: md
        - value: 90 %
          blurb: de temps en moins consacré à la maintenance du système
          icon:
            name: arrow-down
            size: md
            variant: product
    - title: Combien vous coûte votre chaîne d'outils ?
      button:
        text: Calculer votre retour sur investissement
        href: '/calculator/roi/'
        data_ga_name: roi calculator
        data_ga_location: body
      icon:
        name: time-is-money-alt
        size: md
        variant: marketing
    - title: Vous faites vos premiers pas avec GitLab et vous ne savez pas par où commencer ? 
      button:
        text: Explorer les ressources
        href: '/get-started/'
        data_ga_name: get started
        data_ga_location: body
      icon:
        name: documents
        size: md
        variant: marketing
  tabbed_features:
    header: DevSecOps
    blurb: De la planification à la production, GitLab réunit votre équipe
    tabs:
      - header: Développement
        header_mobile: Dev
        data_ga_name: development
        data_ga_location: devsecops
        blurb: Les tâches automatisées améliorent l'efficacité et libèrent le temps des développeurs, sans sacrifier la sécurité.
        features:
          - icon:
              name: continuous-integration
              size: md
              variant: marketing
            text: Intégration et livraison continues
            href: '/features/continuous-integration/'
            data_ga_name: ci
            data_ga_location: development
          - text: Flux de travail alimentés par l'IA avec GitLab Duo
            icon:
              name: ai-code-suggestions
              size: md
              variant: marketing
            href: '/solutions/ai/'
            data_ga_name: ai
            data_ga_location: development
          - text: Gestion du code source
            icon:
              name: cog-code
              size: md
              variant: marketing
            href: '/solutions/source-code-management/'
            data_ga_name: source code management
            data_ga_location: development
          - text: La livraison logicielle automatisée
            icon:
              name: automated-code
              size: md
              variant: marketing
            href: '/solutions/delivery-automation/'
            data_ga_name: automated software delivery
            data_ga_location: development
      - header: Sécurité
        header_mobile: Sec
        blurb: Gérez les vecteurs de menaces et assurez le respect des normes de conformité, sans compromettre la rapidité.
        data_ga_name: security
        data_ga_location: devsecops
        features:
          - icon:
              name: shield-check-light
              size: md
              variant: marketing
            text: Sécurité et conformité
            href: '/solutions/security-compliance/'
            data_ga_name: security and compliance
            data_ga_location: security
          - text: Sécurité de la chaîne logistique logicielle
            icon:
              name: clipboard-check-alt
              size: md
              variant: marketing
            href: '/solutions/supply-chain/'
            data_ga_name: supply chain
            data_ga_location: security
          - text: Tests et remédiations intégrés
            icon:
              name: monitor-test
              size: md
              variant: marketing
            href: '/solutions/continuous-software-security-assurance/'
            data_ga_name: continuous software security assurance
            data_ga_location: security
          - text: Gestion des vulnérabilités et des dépendances
            icon:
              name: monitor-pipeline
              size: md
              variant: marketing
            href: '/solutions/compliance/'
            data_ga_name: compliance
            data_ga_location: security
        image:
          src: '/nuxt-images/home/code-suggestions.png'
          alt: 'Image de marque des tableaux GitLab'
      - header: Opérations
        header_mobile: Ops
        blurb: 'Automatisez le déploiement de logiciels et optimisez les processus, quel que soit votre environnement : cloud native, multi-cloud ou legacy.'
        data_ga_name: operations
        data_ga_location: devsecops
        image:
          src: '/nuxt-images/home/code-suggestions.png'
          alt: 'Image de marque des tableaux GitLab'
        features:
          - icon:
              name: digital-transformation
              size: md
              variant: marketing
            text: GitOps et infrastructure en tant que code
            href: '/solutions/gitops/'
            data_ga_name: gitops
            data_ga_location: operations
          - text: La livraison logicielle automatisée
            icon:
              name: automated-code
              size: md
              variant: marketing
            href: '/solutions/delivery-automation/'
            data_ga_name: delivery automation
            data_ga_location: operations
          - text: La gestion de la chaîne de valeur
            icon:
              name: ai-value-stream-forecast
              size: md
              variant: marketing
            href: '/solutions/value-stream-management/'
            data_ga_name: value stream management
            data_ga_location: operations
  recognition_spotlight:
    heading: GitLab est la plateforme leader DevSecOps
    stats:
      - value: Plus de 50 %
        blurb: Fortune 100
      - value: Plus de 30 M
        blurb: Utilisateurs enregistrés
    cards:
      - logo: /nuxt-images/logos/gartner-logo.svg
        alt: Logo gartner
        text: 'GitLab est reconnu comme un leader dans le Gartner® Magic Quadrant™ 2023 dédié aux plateformes DevOps'
        link:
          text: Lire le rapport
          href: /gartner-magic-quadrant/
          data_ga_name: gartner
          data_ga_location: analyst
      - logo: /nuxt-images/logos/forrester-logo.svg
        alt: Logo forrester
        text: 'GitLab est reconnu comme unique leader dans le rapport The Forrester Wave™ dédié aux plateformes de livraison de logiciels intégrés, T2 2023'
        link:
          text: Lire le rapport
          href: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023
          data_ga_name: forrester
          data_ga_location: analyst
      - header: 'GitLab se classe parmi les leaders au Classement G2 dans les catégories DevSecOps'
        link:
          text: Ce que les analystes du secteur pensent de GitLab
          href: /analysts/
          data_ga_name: g2
          data_ga_location: analyst
        badges:
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_Leader_Enterprise_Leader.png
            alt: G2 Entreprise Leader - été 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_MomentumLeader_Leader.png
            alt: G2 Momentum Leader - été 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_MostImplementable_Total.svg
            alt: G2 Most Implementable - été 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_BestResults_Total.png
            alt: G2 Best Results - été 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_BestRelationship_Enterprise_Total.png
            alt: G2 Best Relationship Enterprise - été 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_BestRelationship_Mid-Market_Total.svg
            alt: G2 Best Relationship Mid-Market - été 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_EasiestToUse_EaseOfUse.png
            alt: G2 Easiest to use - été 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_BestUsability_Total.png
            alt: G2 Best Usability - été 2023
  quotes_carousel_block:
    controls:
      prev: étude de cas précédente
      next: étude de cas suivante
    quotes:
      - main_img:
          url: /nuxt-images/home/JasonManoharan.png
          alt: Photo de Jason Monoharan
        quote: '« La vision de GitLab, qui consiste à allier la stratégie à la portée du logiciel et au code, est très puissante. J''apprécie les efforts majeurs que GitLab ne cesse de fournir dans sa plateforme. »'
        author: Jason Monoharan
        logo:
          url: /nuxt-images/home/logo_iron_mountain_mono.svg
          alt: ''
        role: Vice-président de la technologie
        statistic_samples:
          - data:
              highlight: 150 000 USD
              subtitle: économies de coûts réalisées par an en moyenne
          - data:
              highlight: 20 heures
              subtitle: gain de temps d'intégration par projet
        url: customers/iron-mountain/
        header: <a href='customers/iron-mountain/' data-ga-name='iron mountain' data-ga-location='home case studies'>Iron Mountain</a> fait évoluer son DevOps avec GitLab Ultimate
        data_ga_name: iron mountain
        data_ga_location: home case studies
        cta_text: En savoir plus
        company: Iron Mountain
      - main_img:
          url: /nuxt-images/home/HohnAlan.jpg
          alt: Photo d'Alan Hohn
        quote: '« En passant à GitLab et en automatisant le déploiement, les équipes de développement sont passées de livraisons mensuelles ou hebdomadaires à des livraisons quotidiennes ou à plusieurs livraisons quotidiennes. »'
        author: Alan Hohn
        logo:
          url: /nuxt-images/logos/lockheed-martin.png
          alt: ''
        role: Directeur de la stratégie logiciels
        statistic_samples:
          - data:
              highlight: 80x
              subtitle: faster CI pipeline builds
          - data:
              highlight: 90 %
              subtitle: de temps en moins consacré à la maintenance du système
        url: /customers/lockheed-martin/
        header: <a href='/customers/lockheed-martin/' data-ga-name='lockheed martin' data-ga-location='home case studies'>Lockheed Martin</a> économise du temps, de l'argent et des efforts technologiques grâce à GitLab
        data_ga_name: lockheed martin case study
        data_ga_location: home case studies
        cta_text: En savoir plus
        company: Lockheed Martin
      - main_img:
          url: /nuxt-images/home/EvanO_Connor.png
          alt: Photo d'Evan O’Connor
        quote: '« L''engagement de GitLab envers sa communauté open source nous permet de travailler directement avec des ingénieurs pour résoudre des problèmes techniques complexes. »'
        author: Evan O’Connor
        logo:
          url: /nuxt-images/home/havenTech.png
          alt: ''
        role: Responsable de l'ingénierie plateforme
        statistic_samples:
          - data:
              highlight: 62 %
              subtitle: des utilisateurs mensuels ont exécuté des jobs de détection des secrets
          - data:
              highlight: 66 %
              subtitle: des utilisateurs mensuels ont exécuté des jobs de scanners sécurisés
        url: /customers/haven-technologies/
        header: <a href='/customers/haven-technologies/' data-ga-name='haven technology' data-ga-location='home case studies'>Haven Technologies</a> a migré vers Kubernetes avec GitLab
        data_ga_name: haven technology
        data_ga_location: home case studies
        cta_text: En savoir plus
        company: Haven Technologies
      - main_img:
          url: /nuxt-images/home/RickCarey.png
          alt: Photo de Rick Carey
        quote: '« Nous avons une expression chez UBS : "Tous les développeurs attendent à la même vitesse". Par conséquent, tout ce que nous pouvons faire pour réduire leur temps d''attente offre une valeur ajoutée. Et, grâce à GitLab, nous pouvons bénéficier de cette parfaite intégration. »'
        author: Rick Carey
        logo:
          url: /nuxt-images/home/logo_ubs_mono.svg
          alt: ''
        role: Directeur de la technologie, UBS
        statistic_samples:
          - data:
              highlight: 1 million
              subtitle: compilations réussies dans les six premiers mois
          - data:
              highlight: 12 000
              subtitle: utilisateurs GitLab actifs
        url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
        header: <a href='https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/' data-ga-name='ubs' data-ga-location='home case studies'>UBS</a> a créé sa propre plateforme DevOps à l'aide de GitLab
        data_ga_name: ubs
        data_ga_location: home case studies
        cta_text: En savoir plus
        company: UBS
      - main_img:
          url: /nuxt-images/home/LakshmiVenkatrama.png
          alt: Photo de Lakshmi Venkatraman
        quote: '« GitLab nous permet de bénéficier d''une excellente collaboration avec les membres de l''équipe et entre les différentes équipes. En tant que chef de projet, la possibilité de suivre un projet ou la charge de travail d''un membre de l''équipe permet d''éviter les retards. Lorsque le projet est terminé, nous pouvons facilement automatiser le packaging des logiciels et fournir les solutions au client. Et avec GitLab, tout le processus se déroule en un seul endroit. »'
        author: Lakshmi Venkatraman
        logo:
          url: /nuxt-images/home/singleron.svg
          alt: ''
        role: Chef de projet
        url: https://www.youtube.com/embed/22nmhrlL-FA
        header: Singleron utilise GitLab pour collaborer sur une plateforme unique afin d'améliorer les soins aux patients
        data_ga_name: singleron video
        data_ga_location: home case studies
        cta_text: Regarder la vidéo
        modal: vrai
        comapny: Singleron Biotechnologies
  top-banner:
    text: L'enquête DevSecOps 2022 est disponible, avec des insights provenant de 5 000 experts DevOps.
    link:
      text: Découvrir l'enquête
      href: '/developer-survey/'
      ga_name: devsecops survey
      icon: gl-arrow-right