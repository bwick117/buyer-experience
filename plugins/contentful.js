import { createClient } from 'contentful';

const isContentPreview =
  process.env.CTF_CONTENT_PREVIEW === 'active' ||
  process.env.CI_COMMIT_BRANCH !== process.env.CI_DEFAULT_BRANCH;
const config = {
  space: process.env.CTF_SPACE_ID,
  accessToken: isContentPreview
    ? process.env.CTF_PREVIEW_ACCESS_TOKEN
    : process.env.CTF_CDA_ACCESS_TOKEN,
  host: isContentPreview ? 'preview.contentful.com' : 'cdn.contentful.com',
};

let client;

// Obtain a Singleton instance of the client
export const getClient = () => {
  if (!client) {
    client = createClient(config);
  }

  return client;
};
