export const CONTENT_TYPES = Object.freeze({
  PAGE: 'page',
  NEXT_STEPS: 'nextSteps',
});
